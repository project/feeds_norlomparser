<?php
// $Id:$

/**
 * @file
 * NORLOM Parser plugin.
 */

/**
 * Feeds parser plugin that parses NORLOM feeds.
 */
class FeedsNORLOMParser extends FeedsParser {

  /**
   * Implementation of FeedsParser::parse().
   */
  public function parse(FeedsImportBatch $batch, FeedsSource $source) {
    //feeds_include_library('norlom_parser.inc', 'norlom_parser');
    $result = norlom_parser_parse($batch->getRaw());
    $batch->setTitle($result['title']);
    $batch->setItems($result['items']);
  }

  /**
   * Return mapping sources.
   */
  public function getMappingSources() {
    $mappings = array(
      'title' => array(
        'name' => t('DLR title'),
        'description' => t('Title of the DLR.'),
      ),
      'external_identifier' => array(
        'name' => t('External Identifier'),
        'description' => t('LOM External identifier of the DLR.'),
      ),
      'description' => array(
        'name' => t('Description'),
        'description' => t('DLR description.'),
      ),
      'keyword' => array(
        'name' => t('Keywords'),
        'description' => t('Keywords relevant for this DLR.'),
      ),
      'objectives' => array(
        'name' => t('Learning Objectives'),
        'description' => t('The objectives behind the DLR'),
      ),
      'language' => array(
        'name' => t('Language'),
        'description' => t('Language ISO code.'),
      ),
      'version' => array(
        'name' => t('Version'),
        'description' => t('Version of the DLR.'),
      ),
      'date' => array(
        'name' => t('Date'),
        'description' => t('Date the DLR is published. IEEE format'),
      ),
      'identifier' => array(
        'name' => t('DLR GUID'),
        'description' => t('A unique identifier for the DLR package.'),
      ),
      'contribute' => array(
        'name' => t('DLR Contributor'),
        'description' => t('The publisher that contributed the package.'),
      ),
      'enduserrole' => array(
        'name' => t('DLR End user role'),
        'description' => t('The intended end user of the DLR.'),
      ),
      'rights' => array(
        'name' => t('Copyrights'),
        'description' => t('The copyright statement of the resource.'),
      ),
      'href' => array(
        'name' => t('Resource link'),
        'description' => t('URL to the resource location.'),
      ),
      'big_thumb' => array(
        'name' => t('Big thumbnail'),
        'description' => t('Reference to Big thumbnail gif image'),
      ),
      'small_thumb' => array(
        'name' => t('Small thumbnail'),
        'description' => t('Reference to Small thumbnail gif image.'),
      ),
      'dlr_file' => array(
        'name' => t('DLR Download link'),
        'description' => t('URL to the DLR zipped file'),
      ),
      'cost' => array(
        'name' => t('DLR Cost'),
        'description' => t('Price of the DLR. no if free.'),
      ),
      'false' => array(
        'name' => t('Always false'),
        'description' => t('Dummy field - always false'),
      ),
      'true' => array(
        'name' => t('Always true'),
        'description' => t('Dummy field - always true'),
      ),
      'user_id' => array(
        'name' => t('Import UID'),
        'description' => t('Importing users UID'),
      ),
      );

   // drupal_set_message("<pre>".print_r($mappings, true)."</pre>");

    return $mappings;

  }

  public function configDefaults() {
    return array(
      'xpath' => '',
    );
  }

  public function configForm(&$form_state) {
    $form = array();
    $form['xpath'] = array(
      '#type' => 'textfield',
      '#title' => t('XPath'),
      '#default_value' => $this->config['xpath'],
      '#description' => t('XPath to locate NORLOM items.'),
    );
    return $form;
  }
}

/**
 * Parse NORLOM file.
 *
 * @param $raw
 *   File contents.
 * @return
 *   An array of the parsed NORLOM file.
 */
function norlom_parser_parse($raw) {

  global $user;

  $feeds = $items = array();
  $xml = @ new SimpleXMLElement($raw);
//  $feeds['title'] = (string)current($xml->xpath('//head/title'));
  $feeds['title'] = (string)($xml->organizations->organization->title);

//  drupal_set_message(t('@type %title has been deleted.', array('@type' => node_get_types('name', $node), '%title' => $node->title)));
//  $feeds['identifier'] = (string)($xml->attributes->identifier);


  $xml->registerXPathNamespace('lom', 'http://ltsc.ieee.org/xsd/LOM');
  $xml->registerXPathNamespace('imsmd', 'http://www.imsglobal.org/xsd/imsmd_v1p2');


  // Adds [identifier]
  // Scans for the last occurrence of identifier and stores this. This is a buggy way of obtaining the DLR identifier.
  //
  foreach($xml->resources->resource->attributes() as $name => $value) {
    $items[$name] = (string)$value;
  }

  // Fixing the bug:
  // Use old identifier and replace with new and correct identifier if found in table feeds_node_item:guid
  // Continue using new identifier.

  // Saving old identifier
  $old_id = $items['identifier'];


  // DEBUG - Change back
  // drupal_set_message("<pre>".print_r($items['identifier'], true)."</pre>");

  //query only returning elements with name string and attribute language is nn-NO
  $result = $xml->xpath("//lom:lom/lom:general/lom:description/lom:string[@language='nn-NO']");


  if ($metadata = $xml->metadata->children('lom', true)) {
    $metadata = $metadata->lom;
    $items['contribute'] = (string)$metadata->lifeCycle->contribute->entity;
    $items['date'] = strtotime((string)$metadata->lifeCycle->date->dateTime);
    $items['version'] = ((string)$metadata->lifeCycle->version->string ? (string)$metadata->lifeCycle->version->string : (string)$metadata->lifeCycle->version->langstring);
  }
  else if ($metadata = $xml->metadata->children('imsmd', true)) {
    $metadata = $metadata->lom;
    $items['contribute'] = (string)$metadata->lifecycle->contribute->centity->vcard;
    $items['date'] = strtotime((string)$metadata->lifecycle->date->dateTime);
    $items['version'] = (string)$metadata->lifecycle->version->string;
  }
//    drupal_set_message("<pre>".print_r($metadata, true)."</pre>");


  $items['external_identifier'] = (string)$metadata->general->identifier->entry;
  $items['title'] = ((string)$metadata->general->title->string ? (string)$metadata->general->title->string : (string)$metadata->general->title->langstring);
  $items['description'] = ((string)$metadata->general->description->string ? (string)$metadata->general->description->string : (string)$metadata->general->description->langstring);
  $items['keyword'] = ((string)$metadata->general->keyword->string ? (string)$metadata->general->keyword->string : (string)$metadata->general->keyword->langstring);
  $items['objectives'] = ((string)$metadata->classification->taxonPath->string ? (string)$metadata->classification->taxonPath->string : (string)$metadata->classification->taxonPath->langstring);
  $items['language'] = (string)$metadata->general->language;
  $items['enduserrole'] = ((string)$metadata->educational->intendedEndUserRole->value ? (string)$metadata->educational->intendedEndUserRole->value : (string)$metadata->educational->intendedenduserrole->value->langstring);
  $items['rights'] = (string)$metadata->rights->description->string;
  $items['cost'] = (string)$metadata->rights->cost->value;

  $items['big_thumb'] = (string)$xml->metadata->big_thumb;
  $items['small_thumb'] = (string)$xml->metadata->small_thumb;
  $items['dlr_file'] = (string)$xml->metadata->dlr_file;

  $items['false'] = false;
  $items['true'] = true;
  $items['user_id'] = $user->uid;


  if ($items['title'] == '') {
    $items['title'] = $feeds['title']; // If no title found, use the feeds title
  }

  // Fix for https://jira.bouvet.no/browse/EPK-269
  // Opbtaining new and correct identifier
  if ($items['identifier'] = (string)$metadata->general->identifier->entry) {
    // Replace old identifier with new one in database
    $result = db_fetch_array(db_query("SELECT nid FROM {feeds_node_item} WHERE guid = '%s'", $old_id ));
    if ($result) {
      drupal_set_message("Identifier <strong>".$old_id."</strong> found at node: ".$result['nid'].". Replacing with new identifier: <strong>".$items['identifier']."</strong>");
      $result = db_query("UPDATE {feeds_node_item} SET guid = '%s' WHERE guid = '%s'", $items['identifier'], $old_id);
//      drupal_set_message("Identifier replaced: <pre>".print_r($result, true)."</pre>");
    }
  }
  else {  // If no identifier found where it should be found
    $items['identifier'] = $old_id; // Restore the old one
  }

  // End fix for EPK-269

//  drupal_set_message("<pre>".print_r($items['identifier'], true)."</pre>");

  $feeds['items'] = array($items);
//    drupal_set_message("<pre>".print_r($metadata->general, true)."</pre>");
  return $feeds;
}

function norlom_parser_getSourceElement($item, $element_key) {
  $xml = new SimpleXMLElement($item);
  $xml = $xml->xpath($element_key);
  // Return the first scalar value found after applying xpath
  if (is_array($xml)) {
    foreach ((array)$xml[0] as $value) {
      if (is_scalar($value)) return $value;
    }
  }
  return '';
}

